// import { tokenKey } from '@angular/core/src/view';
import 'rxjs/add/operator/map';
import { Injectable ,Input , Inject, Injector } from '@angular/core';
// import { Http } from '@angular/http';

import { BehaviorSubject, Observable, Subscriber, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { AboutUs } from '../model/AboutUs';
import { Pipe, PipeTransform } from '@angular/core';
import { User } from './User';
import { retry, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import * as $ from 'jquery';
// import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';


// import { TranslateService } from '@ngx-translate/core';


let langCode
let noQuotes;
let langId;
let lang_id

@Injectable({
  providedIn: 'root'
})
export class Constants {
  lang;

  public url: string = "http://159.89.19.28/api/";
  // public faqUrl: string = this.url + "FAQs/viewAllFAQs";
  public registerUrl:string=this.url + "register";
  public loginUrl:string=this.url + "login";
  public aboutUaUrl:string=this.url +"aboutus";
  public faqUrl:string=this.url + "faq";
  constructor(private http: HttpClient, @Inject(Router) private router: Router,
    private translate: TranslateService,private toastrService: ToastrService
  ) {
    
     lang_id = localStorage.getItem("langId");
    if (lang_id == null) {
      localStorage.setItem("langId", JSON.stringify("1"))
      noQuotes = localStorage.getItem("langId").split('"').join('');
    }
    else {
      noQuotes = lang_id.split('"').join('');
    }
    // this.translateMethod()
  }

  translateMethod() {
    this.translate.setDefaultLang('en');
    this.translate.addLangs(['en', 'ar']);
    let y = localStorage.getItem("selected");
    var langCode = y.split('"').join('');
    this.translate.use(langCode);
    let lang_id = localStorage.getItem("langId");
    langId = lang_id.split('"').join('');
    if (Number(langId) == 1) {
      this.lang = true;
    } else {
      this.lang = false;
    }

  }
  //post
  public registerMethod(data): Observable<any> {  
    return this.http.post(this.registerUrl, data ).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  public loginMethod(data): Observable<any> {  
    return this.http.post(this.loginUrl, data ).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
 public aboutUsMethod(){
   return this.http.get(this.aboutUaUrl)
 }
 public faqMethod(){
     return this.http.get(this.faqUrl)
 }

 
 

 

//save user
public  SaveUser(user : User) : boolean
{ 
    
    try {
    
    localStorage.setItem("user", JSON.stringify(user));
}
    catch (e) {
        return false;
    }
    return true;
}

public   getUser(): User {
     return JSON.parse(localStorage.getItem("user")) as User;
}

public   clearUser(): boolean{
    try {            
        localStorage.removeItem("user");
    }
    catch (e) {
        return false;
    }
    return true;
}

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof HttpErrorResponse) {
      // client-side error         
      errorMessage = `Error: ${error.error.error}`;
    } else {
      if (error.error.error == 'duplicated name ') {
        errorMessage = `Name Aleady Exit ... Enter Anther One `;

      } else {
        errorMessage = `Error: ${error.error.error}`;
      }
    }
    return throwError(errorMessage);
  }

}






