import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { Constants } from 'src/app/model/constants';
import { Router } from '@angular/router';
let  langId;
let formData;

@Component({
  selector: 'app-blog-grid-two-in-row-page',
  templateUrl: './blog-grid-two-in-row-page.component.html',
  styleUrls: ['./blog-grid-two-in-row-page.component.scss']
})
export class BlogGridTwoInRowPageComponent implements OnInit {

  lang;
  submitted = false;
  loading:boolean=false;
  loginForm: FormGroup;
  userInfo
    constructor(private translate: TranslateService,private fb: FormBuilder,private toastrService: ToastrService ,  
       private formBuilder: FormBuilder, private router: Router,private service:Constants) { 
        formData = new FormData();
        this.loginForm = this.formBuilder.group({
          // name: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
          email: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required , Validators.minLength(8)]],

         
        })  
       }
  
    ngOnInit(): void {
      this.translateMethod();
    }
    get f() {
      return this.loginForm.controls;
    }
    translateMethod() {
      this.translate.setDefaultLang('en');
      this.translate.addLangs(['en', 'ar']);
      let y = localStorage.getItem("selected");
      var langCode = y.split('"').join('');
      this.translate.use(langCode);
      let lang_id = localStorage.getItem("langId");
      langId = lang_id.split('"').join('');
      if (Number(langId) == 1) {
        this.lang = true;
      } else {
        this.lang = false;
      }
  
    }
    login() {
  
      this.loading = true;
  
      var email =  this.f.email.value
      var password =this.f.password.value
      formData.append("email",email);
      formData.append("password", password);
    
  
      formData.forEach((value,key) => {
        console.log(key+" "+value)
      });
  
      this.submitted = true;
    
      if (this.loginForm.invalid) {
        console.log("bbbb");
        this.loading =false;
        return;
      }
  this.service.loginMethod(formData).subscribe(res=>{
    console.log("login",res)
    this.userInfo = res;
    this.loading = false;
    if (Number(langId) == 1) {
      this.toastrService.success("يرجى مراجعة بريديك الإلكتروني وإدخال رمز التحقق لمتابعة تسجيل الدخول");
    } else {
      this.toastrService.success("Please check your email and enter the verification code to continue logging in");
    }
  
    // this.router.navigate(['/verifyEmail']).then(() => {
    //   window.location.reload();
    
          
          
       
   
          
              
            // });
  
        },(err) => {
            this.loading=false;

         
        
  });  
    }
  }
  