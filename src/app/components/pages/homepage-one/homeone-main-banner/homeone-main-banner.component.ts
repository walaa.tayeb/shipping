import { Component, OnInit } from '@angular/core';
declare let $: any;
@Component({
  selector: 'app-homeone-main-banner',
  templateUrl: './homeone-main-banner.component.html',
  styleUrls: ['./homeone-main-banner.component.scss']
})
export class HomeoneMainBannerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  ngAfterViewInit(){
    $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
}
}
