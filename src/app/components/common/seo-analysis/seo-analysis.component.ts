import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { Constants } from 'src/app/model/constants';
import { Router } from '@angular/router';
let  langId;
let formData;

@Component({
  selector: 'app-seo-analysis',
  templateUrl: './seo-analysis.component.html',
  styleUrls: ['./seo-analysis.component.scss']
})
export class SeoAnalysisComponent implements OnInit {

  lang;
  submitted = false;
  loading:boolean=false;
  registerForm: FormGroup;
  userInfo
    constructor(private translate: TranslateService,private fb: FormBuilder,private toastrService: ToastrService ,  
       private formBuilder: FormBuilder, private router: Router,private service:Constants) { 
        formData = new FormData();
        this.registerForm = this.formBuilder.group({
          name: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
          email: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required , Validators.minLength(8)]],
          password_confirmation: ['', Validators.required],
         
        })  
       }
  
    ngOnInit(): void {
      this.translateMethod();
    }
    get f() {
      return this.registerForm.controls;
    }
    translateMethod() {
      this.translate.setDefaultLang('en');
      this.translate.addLangs(['en', 'ar']);
      let y = localStorage.getItem("selected");
      var langCode = y.split('"').join('');
      this.translate.use(langCode);
      let lang_id = localStorage.getItem("langId");
      langId = lang_id.split('"').join('');
      if (Number(langId) == 1) {
        this.lang = true;
      } else {
        this.lang = false;
      }
  
    }
    register() {
  
      this.loading = true;
  
    
      var name =this.f.name.value;
      var email =  this.f.email.value
      var password =this.f.password.value
      var password_confirmation =this.f.password_confirmation.value
   
  if(password!=password_confirmation){
       
    if (Number(langId) == 1){
      this.loading=false;
      this.toastrService.error('تأكد من تطابق كلمتي المرور','',
      {
        timeOut:8000
      });
  
    }else{
           this.loading=false
      this.toastrService.error(' Make sure your passwords match','',  {
        timeOut:8000
      });
      
  
    }
   
  }
  
  
      formData.append("name", name);
      formData.append("email",email);
      formData.append("password", password);
      formData.append("password_confirmation", password_confirmation);
  
      formData.forEach((value,key) => {
        console.log(key+" "+value)
      });
  
      this.submitted = true;
    
      if (this.registerForm.invalid) {
        console.log("bbbb");
        this.loading =false;
        return;
      }
  this.service.registerMethod(formData).subscribe(res=>{
    console.log("register",res)
    this.userInfo = res;
    this.loading = false;
    if (Number(langId) == 1) {
      this.toastrService.success("يرجى مراجعة بريديك الإلكتروني وإدخال رمز التحقق لمتابعة تسجيل الدخول");
    } else {
      this.toastrService.success("Please check your email and enter the verification code to continue logging in");
    }
  
    // this.router.navigate(['/verifyEmail']).then(() => {
    //   window.location.reload();
    
          
          
       
   
          
              
            // });
  
        },(err) => {
            this.loading=false;

         
        
  });  
    }
  }
  